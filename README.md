# snes

based on https://archive.org/details/nointro.snes (20210119-061911)


ROM not added :
```sh
❯ tree -hs --dirsfirst
.
├── [4.0K]  Cheating Cartridges
│   ├── [ 19K]  Game Genie (USA) (Alt 1) (Unl) [b].7z
│   ├── [ 19K]  Game Genie (USA) (Unl) [b].7z
│   ├── [7.8K]  Pro Action Replay (Europe) (Unl) [b].7z
│   ├── [9.3K]  Pro Action Replay MK2 (Europe) (Unl) [b].7z
│   └── [9.3K]  Pro Action Replay MK2 (Europe) (v1.1) (Unl) [b].7z
├── [4.0K]  Enhancement Chip
│   ├── [3.6K]  DSP1 B (World) (Enhancement Chip).7z
│   ├── [3.6K]  DSP1 (World) (Enhancement Chip).7z
│   ├── [2.5K]  DSP2 (World) (Enhancement Chip).7z
│   ├── [3.2K]  DSP3 (Japan) (Enhancement Chip).7z
│   ├── [3.0K]  DSP4 (World) (Enhancement Chip).7z
│   ├── [2.2K]  ST010 (Japan, USA) (Enhancement Chip).7z
│   ├── [ 14K]  ST011 (Japan) (Enhancement Chip).7z
│   ├── [ 52K]  ST018 (Japan) (Enhancement Chip).7z
│   ├── [ 469]  Super Game Boy 2 SGB2-CPU (Japan) (Enhancement Chip).7z
│   └── [ 461]  Super Game Boy SGB-CPU (World) (Enhancement Chip).7z
├── [ 12K]  Virtual Console
│   ├── [625K]  ActRaiser (Japan) (Virtual Console).7z
│   ├── [788K]  Akumajou Dracula (Japan) (Virtual Console).7z
│   ├── [1.8M]  Bahamut Lagoon (Japan) (Virtual Console).7z
│   ├── [1.5M]  Breath of Fire II - Shimei no Ko (Japan) (Rev 1) (Virtual Console, Switch Online).7z
│   ├── [1.5M]  Breath of Fire II (USA, Europe) (Virtual Console, Switch Online).7z
│   ├── [593K]  Chou Makaimura (Japan) (Virtual Console, Switch Online).7z
│   ├── [685K]  Contra III - The Alien Wars (USA) (Virtual Console).7z
│   ├── [2.7M]  Donkey Kong Country 2 - Diddy's Kong Quest (Europe) (En,Fr) (Rev 1) (Virtual Console).7z
│   ├── [2.7M]  Donkey Kong Country 2 - Diddy's Kong Quest (Germany) (En,De) (Rev 1) (Virtual Console).7z
│   ├── [2.7M]  Donkey Kong Country 2 - Diddy's Kong Quest (USA) (En,Fr) (Rev 1) (Virtual Console).7z
│   ├── [2.2M]  Donkey Kong Country (USA, Europe) (Rev 2) (Virtual Console, Classic Mini, Switch Online).7z
│   ├── [1.7M]  Famicom Bunko - Hajimari no Mori (Japan) (Virtual Console).7z
│   ├── [600K]  Final Fantasy II (USA, Europe) (Rev 1) (Virtual Console).7z
│   ├── [603K]  Final Fantasy IV (Japan) (Rev 1) (Virtual Console).7z
│   ├── [728K]  Final Fight 2 (Europe) (Virtual Console).7z
│   ├── [728K]  Final Fight 2 (USA) (Virtual Console).7z
│   ├── [1.6M]  Final Fight 3 (USA) (Virtual Console).7z
│   ├── [568K]  Final Fight (Europe) (Virtual Console).7z
│   ├── [566K]  Final Fight (Japan) (Virtual Console).7z
│   ├── [1.6M]  Final Fight Tough (Japan) (Virtual Console).7z
│   ├── [568K]  Final Fight (USA) (Virtual Console).7z
│   ├── [2.2M]  Fushigi no Dungeon 2 - Fuurai no Shiren (Japan) (Rev 1) (Virtual Console).7z
│   ├── [249K]  F-Zero (Japan) (En) (Virtual Console, Switch Online).7z
│   ├── [249K]  F-Zero (USA, Europe) (Virtual Console, Classic Mini, Switch Online).7z
│   ├── [1.4M]  Ganbare Goemon 2 - Kiteretsu Shougun Magginesu (Japan) (Virtual Console).7z
│   ├── [1.4M]  Ganbare Goemon 3 - Shishi Juurokubee no Karakuri Manjigatame (Japan) (Rev 2) (Virtual Console).7z
│   ├── [792K]  Ganbare Goemon - Yuki Hime Kyuushutsu Emaki (Japan) (Rev 2) (Virtual Console).7z
│   ├── [1.6M]  Heracles no Eikou IV - Kamigami kara no Okurimono (Japan) (Virtual Console).7z
│   ├── [557K]  Kirby Bowl (Japan) (Virtual Console, Switch Online).7z
│   ├── [524K]  Kirby's Dream Course (Europe) (Virtual Console).7z
│   ├── [524K]  Kirby's Dream Course (USA, Europe) (Virtual Console, Switch Online).7z
│   ├── [787K]  Legend of the Mystical Ninja, The (USA) (Virtual Console).7z
│   ├── [351K]  Mario's Super Picross (Europe) (Ja) (Virtual Console).7z
│   ├── [1.1M]  Ogre Battle - The March of the Black Queen (USA, Europe) (Virtual Console).7z
│   ├── [651K]  Panel de Pon (World) (Ja) (Rev 1) (Virtual Console, Switch Online).7z
│   ├── [590K]  Romancing Sa-Ga (Japan) (Rev 1) (Virtual Console).7z
│   ├── [789K]  R-Type III (Europe) (Virtual Console).7z
│   ├── [721K]  R-Type III - The Third Lightning (Japan) (En) (Virtual Console).7z
│   ├── [791K]  R-Type III (USA) (Virtual Console).7z
│   ├── [1.2M]  Secret of Mana (USA) (Virtual Console).7z
│   ├── [1.2M]  Seiken Densetsu 2 (Japan) (Rev 1) (Virtual Console).7z
│   ├── [1.1M]  Street Fighter II (Europe) (Virtual Console).7z
│   ├── [1.1M]  Street Fighter II (Japan) (Virtual Console).7z
│   ├── [1.5M]  Street Fighter II Turbo (Europe) (Rev 1) (Virtual Console).7z
│   ├── [1.5M]  Street Fighter II Turbo (Japan) (Rev 1) (Virtual Console).7z
│   ├── [1.5M]  Street Fighter II Turbo (USA) (Rev 1) (Virtual Console).7z
│   ├── [1.1M]  Street Fighter II (USA) (Virtual Console).7z
│   ├── [786K]  Super Castlevania IV (Europe) (Virtual Console).7z
│   ├── [786K]  Super Castlevania IV (USA) (Virtual Console).7z
│   ├── [2.6M]  Super Donkey Kong 2 - Dixie & Diddy (Japan) (Rev 1) (Virtual Console).7z
│   ├── [2.2M]  Super Donkey Kong (Japan) (Rev 1) (Virtual Console, Classic Mini, Switch Online).7z
│   ├── [593K]  Super Ghouls 'N Ghosts (USA, Europe) (Virtual Console, Classic Mini, Switch Online).7z
│   ├── [2.3M]  Super Mario RPG (Japan) (Virtual Console).7z
│   ├── [2.3M]  Super Mario RPG - Legend of the Seven Stars (USA, Europe) (Virtual Console).7z
│   ├── [308K]  Super Mario World (USA, Europe) (Virtual Console, Classic Mini, Switch Online).7z
│   ├── [1.2M]  Super Metroid (Japan) (En,Ja) (Virtual Console, Switch Online).7z
│   ├── [1.2M]  Super Metroid (USA, Europe) (En,Ja) (Virtual Console, Classic Mini, Switch Online).7z
│   ├── [682K]  Super Probotector - Alien Rebels (Europe) (Virtual Console).7z
│   ├── [2.5M]  Super Street Fighter II (Europe) (Virtual Console).7z
│   ├── [2.5M]  Super Street Fighter II - The New Challengers (Japan) (Virtual Console).7z
│   ├── [2.5M]  Super Street Fighter II (USA) (Rev 1) (Virtual Console).7z
│   ├── [359K]  Super Turrican (Europe) (Virtual Console).7z
│   ├── [358K]  Super Turrican (USA) (Virtual Console).7z
│   ├── [2.1M]  Tactics Ogre - Let Us Cling Together (Japan) (Rev 1) (Virtual Console).7z
│   ├── [1.9M]  Treasure Hunter G (Japan) (Virtual Console).7z
│   ├── [618K]  Zelda no Densetsu - Kamigami no Triforce (Japan) (Rev 2) (Virtual Console, Switch Online).7z
│   ├── [527K]  Zombies Ate My Neighbors (USA) (Virtual Console).7z
│   └── [524K]  Zombies (Europe) (Virtual Console).7z
├── [223K]  Batman (USA) (Proto) [b].7z
├── [383K]  Bug's Life, A (World) (Pirate).7z
├── [ 78K]  Creepy Bird (USA) (Unl).7z
├── [793K]  From TV Animation Slam Dunk - Shuueisha Limited (Japan) (Possible Proto).7z
├── [340K]  Hind Strike (USA) (Unl) [b].7z
├── [876K]  Iron Commando (Europe) (Proto) [b].7z
├── [651K]  Lost Vikings, The (USA) (Beta 1) [b].7z
├── [349K]  MACS Basic Rifle Marksmanship (USA) [b].7z
├── [1.1M]  Mick & Mack as the Global Gladiators (USA) (Proto) [b].7z
├── [2.6M]  Mortal Kombat II (USA) (Beta) [b].7z
├── [707K]  Picachu ~ Pocket Monsters (World) (Pirate).7z
├── [1.4M]  P.T.O. II - Pacific Theater of Operations (USA).7z
├── [604K]  P.T.O. - Pacific Theater of Operations (USA).7z
├── [379K]  Rally - The Final Round of the World Rally Championship (USA) (Proto).7z
├── [354K]  SM Choukyoushi Hitomi (Japan) (Beta) (Unl) [b].7z
├── [333K]  SM Choukyoushi Hitomi Vol. 3 (Japan) (Alt 1) (Unl) [b].7z
├── [371K]  SM Choukyoushi Hitomi Vol. 3 (Japan) (Unl) [b].7z
├── [310K]  Super Mario World (Japan) (En) (Arcade) [b].7z
├── [509K]  Super Putty (Europe) (Beta) [b].7z
├── [ 13K]  Super X-Terminator 2 Sasuke (Japan) (Unl) [b].7z
├── [427K]  Tony Meola's Sidekicks Soccer (USA) (Beta 3) [b].7z
└── [302K]  Untitled Racing Game (Unknown) (Proto).7z

3 directories, 105 files
```